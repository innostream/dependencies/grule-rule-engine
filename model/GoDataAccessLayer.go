//  Copyright hyperjumptech/grule-rule-engine Authors
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

package model

import (
	"fmt"
	"reflect"

	"gitlab.com/innostream/dependencies/grule-rule-engine/pkg"
)

// NewGoValueNode creates new instance of ValueNode backed by golang reflection
func NewGoValueNode(value reflect.Value, identifiedAs string) ValueNode {

	return &GoValueNode{
		parentNode:   nil,
		identifiedAs: identifiedAs,
		thisValue:    value,
	}
}

// GoValueNode is an implementation of ValueNode that used to traverse native golang primitives through reflect package
type GoValueNode struct {
	parentNode   ValueNode
	identifiedAs string
	thisValue    reflect.Value
}

// Value \n\nreturns the underlying reflect.Value
func (node *GoValueNode) Value() reflect.Value {

	return node.thisValue
}

// HasParent \n\nreturns `true` if the current value is a field, function, map, array, slice of another value
func (node *GoValueNode) HasParent() bool {

	return node.parentNode != nil
}

// Parent \n\nreturns the value node of the parent value, if this node is a field, function, map, array, slice of another value
func (node *GoValueNode) Parent() ValueNode {

	return node.parentNode
}

// IdentifiedAs \n\nreturn the current representation of this Value Node
func (node *GoValueNode) IdentifiedAs() string {
	if node.HasParent() {
		if node.parentNode.IsArray() || node.parentNode.IsMap() {

			return fmt.Sprintf("%s%s", node.parentNode.IdentifiedAs(), node.identifiedAs)
		}

		return fmt.Sprintf("%s.%s", node.parentNode.IdentifiedAs(), node.identifiedAs)
	}

	return node.identifiedAs
}

// ContinueWithValue will return a nother ValueNode to wrap the specified value and treated as child of current node.
// The main purpose of this is for easier debugging.
func (node *GoValueNode) ContinueWithValue(value reflect.Value, identifiedAs string) ValueNode {

	return &GoValueNode{
		parentNode:   node,
		identifiedAs: identifiedAs,
		thisValue:    value,
	}
}

// GetValue will \n\nreturn the underlying reflect.Value
func (node *GoValueNode) GetValue() (reflect.Value, error) {

	return node.thisValue, nil
}

// GetType will \n\nreturn the underlying value's type
func (node *GoValueNode) GetType() (reflect.Type, error) {

	return node.thisValue.Type(), nil
}

// IsArray to check if the underlying value is an array or not
func (node *GoValueNode) IsArray() bool {

	return node.thisValue.Kind() == reflect.Array || node.thisValue.Kind() == reflect.Slice
}

func (node *GoValueNode) IsInterface() bool {

	return node.thisValue.Kind() == reflect.Interface
}

// GetArrayType to get the type of underlying value array element types.
func (node *GoValueNode) GetArrayType() (reflect.Type, error) {
	if node.IsArray() {

		return node.thisValue.Type().Elem(), nil
	}

	return nil, fmt.Errorf("this node identified as \"%s\" is not referring to an array or slice", node.IdentifiedAs())
}

// GetArrayValueAt to get the value of an array element if the current underlying value is an array
func (node *GoValueNode) GetArrayValueAt(index int) (val reflect.Value, err error) {
	if node.IsArray() {
		defer func() {
			if r := recover(); r != nil {
				err = fmt.Errorf("recovered : %v", r)
			}
		}()

		return node.thisValue.Index(index), err
	}

	return reflect.Value{}, fmt.Errorf("this node identified as \"%s\" is not referring to an array or slice", node.IdentifiedAs())
}

// GetChildNodeByIndex is similar to `GetArrayValueAt`, where this will \n\nreturn a ValueNode that wrap the value.
func (node *GoValueNode) GetChildNodeByIndex(index int) (ValueNode, error) {
	if node.IsArray() {
		v, err := node.GetArrayValueAt(index)
		if err != nil {

			return nil, err
		}
		gv := node.ContinueWithValue(v, fmt.Sprintf("[%d]", index))

		return gv, nil
	}

	return nil, fmt.Errorf("this node identified as \"%s\" is not an array. its %s", node.IdentifiedAs(), node.thisValue.Type().String())
}

// SetArrayValueAt will set the value of specified array index on the current underlying array value.
func (node *GoValueNode) SetArrayValueAt(index int, value reflect.Value) (err error) {
	if node.IsArray() {
		defer func() {
			if r := recover(); r != nil {
				err = fmt.Errorf("recovered : %v", r)
			}
		}()
		val := node.thisValue.Index(index)
		if val.CanAddr() && val.CanSet() {
			if pkg.IsNumber(val) && pkg.IsNumber(value) {

				return SetNumberValue(val, value)
			}
			val.Set(value)

			return nil
		}

		return fmt.Errorf("this node identified as \"%s\" can not set value on array index %d", node.IdentifiedAs(), index)
	}

	return fmt.Errorf("this node identified as \"%s\" is not referencing an array or slice", node.IdentifiedAs())
}

// AppendValue will append the new values into the current underlying slice.
// It will return error if argument list are not compatible with the slice element type.
func (node *GoValueNode) AppendValue(value []reflect.Value) (err error) {
	elem := node.thisValue
	if (elem.Kind() == reflect.Ptr || elem.Kind() == reflect.Interface) && (elem.Elem().Kind() == reflect.Array || elem.Elem().Kind() == reflect.Slice) {
		elem = elem.Elem()
	}
	if elem.Kind() == reflect.Array || elem.Kind() == reflect.Slice {
		if elem.CanSet() {
			defer func() {
				if r := recover(); r != nil {
					err = fmt.Errorf("recovered : %v", r)
				}
			}()
			elem.Set(reflect.Append(elem, value...))

			return nil
		} else if node.parentNode.Value().Kind() == reflect.Map {
			defer func() {
				if r := recover(); r != nil {
					err = fmt.Errorf("recovered : %v", r)
				}
			}()
			sourceMap := node.parentNode.Value()
			sourceMap.SetMapIndex(reflect.ValueOf(node.identifiedAs), reflect.Append(elem, value...))
		}
	}

	return fmt.Errorf("this node identified as \"%s\" is not referencing an array or slice", node.IdentifiedAs())
}

// RemoveValue will remove the provided values from the current underlying slice.
// It will return error if argument list are not compatible with the slice element type.
func (node *GoValueNode) RemoveValue(value []reflect.Value) (err error) {
	elem := node.thisValue
	if (elem.Kind() == reflect.Ptr || elem.Kind() == reflect.Interface) && (elem.Elem().Kind() == reflect.Array || elem.Elem().Kind() == reflect.Slice) {
		elem = elem.Elem()
	}
	if elem.Kind() == reflect.Array {
		elem = elem.Slice(0, elem.Len()-1)
	}
	if elem.Kind() == reflect.Slice {
		working := elem
		for i := 0; i < working.Len(); i++ {
			val := working.Index(i)
			remove := false
			for _, toRemove := range value {
				if val.Equal(toRemove) {
					remove = true
				}
			}
			if remove {
				working = reflect.AppendSlice(working.Slice(0, i), working.Slice(i+1, working.Len()))
				i--
			}
		}

		if elem.CanSet() {
			defer func() {
				if r := recover(); r != nil {
					err = fmt.Errorf("recovered : %v", r)
				}
			}()
			elem.Set(working)

			return nil
		} else if node.parentNode.Value().Kind() == reflect.Map {
			defer func() {
				if r := recover(); r != nil {
					err = fmt.Errorf("recovered : %v", r)
				}
			}()
			sourceMap := node.parentNode.Value()
			sourceMap.SetMapIndex(reflect.ValueOf(node.identifiedAs), working)
		}
	}

	return fmt.Errorf("this node identified as \"%s\" is not referencing an array or slice", node.IdentifiedAs())
}

// Clear empties a map or array/slice
func (node *GoValueNode) Clear() (err error) {
	elem := node.thisValue
	if (elem.Kind() == reflect.Ptr || elem.Kind() == reflect.Interface) && (elem.Elem().Kind() == reflect.Array || elem.Elem().Kind() == reflect.Slice || elem.Elem().Kind() == reflect.Map) {
		elem = elem.Elem()
	}

	if elem.CanSet() {
		defer func() {
			if r := recover(); r != nil {
				err = fmt.Errorf("recovered : %v", r)
			}
		}()
		if elem.Kind() == reflect.Array || elem.Kind() == reflect.Slice {
			elem.SetLen(0)
		}

		if elem.Kind() == reflect.Map {
			elem.Clear()
		}

		return nil
	} else if node.parentNode.Value().Kind() == reflect.Map {
		defer func() {
			if r := recover(); r != nil {
				err = fmt.Errorf("recovered : %v", r)
			}
		}()

		if elem.Kind() == reflect.Array || elem.Kind() == reflect.Slice {
			sourceMap := node.parentNode.Value()
			sourceMap.SetMapIndex(reflect.ValueOf(node.identifiedAs), reflect.MakeSlice(elem.Type(), 0, 0))
		}

		if elem.Kind() == reflect.Map {
			sourceMap := node.parentNode.Value()
			sourceMap.SetMapIndex(reflect.ValueOf(node.identifiedAs), reflect.MakeMap(elem.Type()))
		}
	}

	return fmt.Errorf("this node identified as \"%s\" is not referencing an array or slice", node.IdentifiedAs())
}

// Length will \n\nreturn the length of underlying value if its an array, slice, map or string
func (node *GoValueNode) Length() (int, error) {
	if node.IsArray() || node.IsMap() || node.IsString() {

		return node.thisValue.Len(), nil
	}

	return 0, fmt.Errorf("this node identified as \"%s\" is not referencing an array, slice, map or string", node.IdentifiedAs())
}

// IsMap will validate if the underlying value is a map.
func (node *GoValueNode) IsMap() bool {

	return node.thisValue.Kind() == reflect.Map
}

// GetMapValueAt will retrieve a map value by the specified key argument.
func (node *GoValueNode) GetMapValueAt(index reflect.Value, allowedMissingKey bool) (reflect.Value, error) {
	if node.IsMap() {
		retVal := node.thisValue.MapIndex(index)
		if retVal.IsValid() {

			return retVal, nil
		}

		if allowedMissingKey {
			mapType := node.thisValue.Type().Elem()
			return reflect.Zero(mapType), nil
		}

		return reflect.Value{}, fmt.Errorf("this node identified as \"%s\" have no selector with specified key", node.IdentifiedAs())
	}

	return reflect.Value{}, fmt.Errorf("this node identified as \"%s\" is not referencing a map", node.IdentifiedAs())
}

// SetMapValueAt will set the map value for the specified key, value argument
func (node *GoValueNode) SetMapValueAt(index, newValue reflect.Value) (err error) {
	if node.IsMap() {
		defer func() {
			if r := recover(); r != nil {
				err = fmt.Errorf("recovered : %v", r)
			}
		}()
		node.thisValue.SetMapIndex(index, newValue)

		return nil
	}

	return fmt.Errorf("this node identified as \"%s\" is not referencing a map", node.IdentifiedAs())
}

// GetChildNodeBySelector is similar to GetMapValueAt, it retrieve a value of map element identified by a value index as ValueNode.
func (node *GoValueNode) GetChildNodeBySelector(index reflect.Value, allowedMissingKey bool) (ValueNode, error) {
	val, err := node.GetMapValueAt(index, allowedMissingKey)
	if err != nil {

		return nil, err
	}

	return node.ContinueWithValue(val, index.String()), nil
}

// IsObject will check if the underlying value is a struct or pointer to a struct
func (node *GoValueNode) IsObject() bool {
	if node.thisValue.IsValid() {
		typ := node.thisValue.Type()
		if typ.Kind() == reflect.Ptr {

			return typ.Elem().Kind() == reflect.Struct
		}

		return typ.Kind() == reflect.Struct
	}

	return false
}

// GetObjectValueByField will \n\nreturn underlying value's field
func (node *GoValueNode) GetObjectValueByField(field string) (reflect.Value, error) {
	if node.IsObject() {
		var val reflect.Value
		if node.thisValue.Kind() == reflect.Ptr {
			val = node.thisValue.Elem().FieldByName(field)
		}
		if node.thisValue.Kind() == reflect.Struct {
			val = node.thisValue.FieldByName(field)
		}
		if val.IsValid() {

			return val, nil
		}

		return reflect.Value{}, fmt.Errorf("this node have no field named %s", field)
	}

	return reflect.Value{}, fmt.Errorf("this node identified as \"%s\" is not referencing to an object", node.IdentifiedAs())
}

// GetObjectTypeByField will return underlying type of the value's field
func (node *GoValueNode) GetObjectTypeByField(field string) (typ reflect.Type, err error) {
	if node.IsObject() {
		defer func() {
			if r := recover(); r != nil {
				err = fmt.Errorf("recovered : %v", r)
				typ = nil
			}
		}()
		if node.thisValue.Kind() == reflect.Ptr {

			return node.thisValue.Elem().FieldByName(field).Type(), nil
		}
		if node.thisValue.Kind() == reflect.Struct {

			return node.thisValue.FieldByName(field).Type(), nil
		}
	}

	return nil, fmt.Errorf("this node identified as \"%s\" is not referring to an object", node.IdentifiedAs())
}

// SetNumberValue will assign a numeric value to a numeric target value
// this helper function is to ensure assignment between numerical types is happening regardless of types, int, uint or float.
// The rule designer should be careful as conversion of types in automatic way like this will cause lost of precision
// during conversion. This will be removed in the future version.
func SetNumberValue(target, newvalue reflect.Value) error {
	if pkg.IsNumber(target) && pkg.IsNumber(newvalue) {
		switch target.Type().Kind() {
		case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
			if pkg.GetBaseKind(newvalue) == reflect.Uint64 {
				target.SetInt(int64(newvalue.Uint()))
			} else if pkg.GetBaseKind(newvalue) == reflect.Float64 {
				target.SetInt(int64(newvalue.Float()))
			} else {
				target.SetInt(newvalue.Int())
			}

			return nil
		case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:
			if pkg.GetBaseKind(newvalue) == reflect.Uint64 {
				target.SetUint(newvalue.Uint())
			} else if pkg.GetBaseKind(newvalue) == reflect.Float64 {
				target.SetUint(uint64(newvalue.Float()))
			} else {
				target.SetUint(uint64(newvalue.Int()))
			}

			return nil
		case reflect.Float32, reflect.Float64:
			if pkg.GetBaseKind(newvalue) == reflect.Uint64 {
				target.SetFloat(float64(newvalue.Uint()))
			} else if pkg.GetBaseKind(newvalue) == reflect.Float64 {
				target.SetFloat(newvalue.Float())
			} else {
				target.SetFloat(float64(newvalue.Int()))
			}

			return nil
		}

		return fmt.Errorf("this line should not be reached")
	}

	return fmt.Errorf("this function only used for assigning number data to number variable")
}

// SetObjectValueByField will set the underlying value's field with new value.
func (node *GoValueNode) SetObjectValueByField(field string, newValue reflect.Value) (err error) {
	fieldVal := node.thisValue.Elem().FieldByName(field)
	if fieldVal.IsValid() && fieldVal.CanAddr() && fieldVal.CanSet() {
		defer func() {
			if r := recover(); r != nil {
				err = fmt.Errorf("recovered : %v", r)
			}
		}()
		if pkg.IsNumber(fieldVal) && pkg.IsNumber(newValue) {

			return SetNumberValue(fieldVal, newValue)
		}
		fieldVal.Set(newValue)

		return nil
	}

	return fmt.Errorf("this node identified as \"%s\" have field \"%s\" that is not valid nor addressable", node.IdentifiedAs(), field)
}

// CallFunction will call a function owned by the underlying value receiver.
// this function will artificially create a built-in functions for constants, array and map.
func (node *GoValueNode) CallFunction(funcName string, args ...reflect.Value) (retval reflect.Value, shouldClearMemory bool, err error) {
	// Check if any of our args are empty interface placeholders, and if so then abort
	for _, arg := range args {
		if !arg.IsValid() {
			return reflect.Value{}, false, nil
		}
		if arg.Kind() == reflect.Interface && arg.NumMethod() == 0 && arg.IsZero() {
			return reflect.Value{}, false, nil
		}
	}
	switch pkg.GetBaseElemKind(node.thisValue) {
	case reflect.Int64, reflect.Uint64, reflect.Float64, reflect.Bool:

		return reflect.ValueOf(nil), false, fmt.Errorf("this node identified as \"%s\" try to call function %s which is not supported for type %s", node.IdentifiedAs(), funcName, node.thisValue.Type().String())
	case reflect.String:
		var strfunc func(string, []reflect.Value) (reflect.Value, error)
		switch funcName {
		case "In":
			strfunc = StrIn
		case "Compare":
			strfunc = StrCompare
		case "Contains":
			strfunc = StrContains
		case "Count":
			strfunc = StrCount
		case "HasPrefix":
			strfunc = StrHasPrefix
		case "HasSuffix":
			strfunc = StrHasSuffix
		case "Index":
			strfunc = StrIndex
		case "LastIndex":
			strfunc = StrLastIndex
		case "Repeat":
			strfunc = StrRepeat
		case "Replace":
			strfunc = StrReplace
		case "Split":
			strfunc = StrSplit
		case "ToLower":
			strfunc = StrToLower
		case "ToUpper":
			strfunc = StrToUpper
		case "Trim":
			strfunc = StrTrim
		case "Len":
			strfunc = StrLen
		case "MatchString":
			strfunc = StrMatchRegexPattern
		}
		if strfunc != nil {
			val, err := strfunc(Stringify(node.thisValue), args)
			if err != nil {

				return reflect.Value{}, false, err
			}

			return val, false, nil
		}

		return reflect.Value{}, false, fmt.Errorf("this node identified as \"%s\" call function %s is not supported for string", node.IdentifiedAs(), funcName)
	}
	if node.IsArray() || node.IsInterface() && (node.thisValue.Elem().Kind() == reflect.Array || node.thisValue.Elem().Kind() == reflect.Slice) {
		var arrFunc func(reflect.Value, []reflect.Value) (reflect.Value, error)
		switch funcName {
		case "Len":
			arrFunc = ArrMapLen
		case "Append":
			node.AppendValue(args)

			return reflect.Value{}, true, nil
		case "Remove":
			node.RemoveValue(args)
			return reflect.Value{}, true, nil
		case "Clear":
			node.Clear()

			return reflect.Value{}, true, nil
		}

		if arrFunc != nil {
			val, err := arrFunc(node.thisValue, args)
			if err != nil {

				return reflect.Value{}, false, err
			}

			return val, false, nil
		}

		return reflect.Value{}, false, fmt.Errorf("this node identified as \"%s\" call function %s is not supported for array", node.IdentifiedAs(), funcName)
	}
	if node.IsMap() {
		var mapFunc func(reflect.Value, []reflect.Value) (reflect.Value, error)
		switch funcName {
		case "Len":
			mapFunc = ArrMapLen
		case "Clear":
			node.Clear()

			return reflect.Value{}, true, nil
		}

		if mapFunc != nil {
			val, err := mapFunc(node.thisValue, args)
			if err != nil {

				return reflect.Value{}, false, err
			}

			return val, false, nil
		}

		return reflect.Value{}, false, fmt.Errorf("this node identified as \"%s\" call function %s is not supported for map", node.IdentifiedAs(), funcName)
	}

	if node.IsObject() || node.IsInterface() {
		funcValue := node.thisValue.MethodByName(funcName)
		for i := 0; i < funcValue.Type().NumIn() && i < len(args); i++ {
			expected := funcValue.Type().In(i)
			if args[i].Type() != expected && (args[i].Type().Kind() == reflect.Interface || args[i].Type().Kind() == reflect.Pointer) {
				args[i] = args[i].Elem()
			}
		}

		if funcValue.IsValid() {
			rets := funcValue.Call(args)
			if len(rets) > 1 {

				return reflect.Value{}, false, fmt.Errorf("this node identified as \"%s\" calling function %s which \n\nreturns multiple values, multiple value \n\nreturns are not supported", node.IdentifiedAs(), funcName)
			}
			if len(rets) == 1 {

				return rets[0], false, nil
			}

			return reflect.Value{}, false, nil
		}

		return reflect.Value{}, false, fmt.Errorf("this node identified as \"%s\" have no function named %s", node.IdentifiedAs(), funcName)
	}

	return reflect.ValueOf(nil), false, fmt.Errorf("this node identified as \"%s\" is not referencing an object thus function %s call is not supported. Kind %s", node.IdentifiedAs(), funcName, node.thisValue.Kind().String())
}

// GetChildNodeByField will retrieve the underlying struct's field and \n\nreturn the ValueNode wraper.
func (node *GoValueNode) GetChildNodeByField(field string) (ValueNode, error) {
	val, err := node.GetObjectValueByField(field)
	if err != nil {

		return nil, err
	}

	return node.ContinueWithValue(val, field), nil
}

// IsTime will check if the underlying value is a time.Time
func (node *GoValueNode) IsTime() bool {

	return node.thisValue.Type().String() == "time.Time"
}

// IsInteger will check if the underlying value is a type of int, or uint
func (node *GoValueNode) IsInteger() bool {
	kind := pkg.GetBaseKind(node.thisValue)

	return kind == reflect.Int64 || kind == reflect.Uint64
}

// IsReal will check if the underlying value is a type of real number, float.
func (node *GoValueNode) IsReal() bool {
	kind := pkg.GetBaseKind(node.thisValue)

	return kind == reflect.Float64
}

// IsBool will check if the underlying value is a type of boolean.
func (node *GoValueNode) IsBool() bool {

	return node.thisValue.Kind() == reflect.Bool
}

// IsString will check if the underlying value is a type of string
func (node *GoValueNode) IsString() bool {

	return node.thisValue.Kind() == reflect.String
}

func Stringify(value reflect.Value) string {
	if value.Kind() == reflect.Pointer || value.Kind() == reflect.Interface {
		return Stringify(value.Elem())
	}

	return value.String()
}
