package examples

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/innostream/dependencies/grule-rule-engine/ast"
	"gitlab.com/innostream/dependencies/grule-rule-engine/builder"
	"gitlab.com/innostream/dependencies/grule-rule-engine/engine"
	"gitlab.com/innostream/dependencies/grule-rule-engine/pkg"
)

type Vars struct {
	vars map[string]interface{}
	kbs  *ast.KnowledgeBase
}

func NewVars() *Vars {
	return &Vars{
		vars: make(map[string]interface{}),
	}
}

func (rf *Vars) SetKBS(kbs *ast.KnowledgeBase) {
	rf.kbs = kbs
}

func (rf *Vars) Set(key string, value interface{}) {
	rf.vars[key] = value
	// Grule caches expression evaluations, so we need to reset the cache when we change the vars
	if rf.kbs != nil && rf.kbs.WorkingMemory != nil {
		rf.kbs.WorkingMemory.Reset("Vars.Get")
	}
}

func (rf *Vars) Get(key string) interface{} {
	item := rf.vars[key]
	switch item.(type) {
	case []string:
		return item.([]string)
	case bool:
		return item.(bool)
	case string:
		return item.(string)
	case int64:
		return item.(int64)
	case uint64:
		return item.(uint64)
	case float64:
		return item.(float64)
	}
	return rf.vars[key]
}

func TestVarsFunction(t *testing.T) {
	vars := NewVars()

	dataContext := ast.NewDataContext()
	err := dataContext.Add("Vars", vars)
	assert.NoError(t, err)

	rules := `
	rule Test salience 1000 {
		when 
			true
		then 
			Vars.Set("Key", "Test");
			Retract("Test");
	}
	rule Final salience 500 {
		when 
			Vars.Get("Key").In("Ignore","Test")
		then 
		    Vars.Set("Pass", true);
			Complete();		
	}
`

	// Prepare knowledgebase library and load it with our rule.
	lib := ast.NewKnowledgeLibrary()
	ruleBuilder := builder.NewRuleBuilder(lib)
	err = ruleBuilder.BuildRuleFromResource("TestVarsFunction", "0.1.1", pkg.NewBytesResource([]byte(rules)))
	assert.NoError(t, err)

	knowledgeBase, err := lib.NewKnowledgeBaseInstance("TestVarsFunction", "0.1.1")
	assert.NoError(t, err)
	vars.SetKBS(knowledgeBase)
	eng1 := &engine.GruleEngine{MaxCycle: 500}
	err = eng1.Execute(dataContext, knowledgeBase)
	assert.NoError(t, err)
	assert.True(t, dataContext.IsComplete())
	assert.True(t, vars.vars["Pass"] == true)
	assert.True(t, vars.vars["Key"] == "Test")
}
