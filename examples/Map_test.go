package examples

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/innostream/dependencies/grule-rule-engine/ast"
	"gitlab.com/innostream/dependencies/grule-rule-engine/builder"
	"gitlab.com/innostream/dependencies/grule-rule-engine/engine"
	"gitlab.com/innostream/dependencies/grule-rule-engine/pkg"
)

type StringMapHolder struct {
	Vars map[string]string
}

func TestHolderObjectMapString(t *testing.T) {
	holder := &StringMapHolder{
		Vars: map[string]string{"Color": "Black"},
	}
	dataContext := ast.NewDataContext()
	err := dataContext.Add("Holder", holder)
	assert.NoError(t, err)

	rule := `
	rule SetHolderVarMessage "test 1" {
		when
		  Holder.Vars["Color"].In("Black", "Yellow")
		then
		  Holder.Vars["Message"] = "Its either black or yellow!!!";
		  Retract("SetHolderVarMessage");
	}
	`

	// Prepare knowledgebase library and load it with our rule.
	lib := ast.NewKnowledgeLibrary()
	rb := builder.NewRuleBuilder(lib)
	err = rb.BuildRuleFromResource("Test", "0.1.1", pkg.NewBytesResource([]byte(rule)))
	assert.NoError(t, err)
	kb, err := lib.NewKnowledgeBaseInstance("Test", "0.1.1")
	assert.NoError(t, err)
	eng1 := &engine.GruleEngine{MaxCycle: 1}
	err = eng1.Execute(dataContext, kb)
	assert.NoError(t, err)
	assert.Equal(t, "Its either black or yellow!!!", holder.Vars["Message"])
}

func TestHolderObjectMapStringAccessor(t *testing.T) {
	holder := &StringMapHolder{
		Vars: map[string]string{"Color": "Black"},
	}
	dataContext := ast.NewDataContext()
	err := dataContext.Add("Holder", holder)
	assert.NoError(t, err)

	rule := `
	rule SetHolderVarMessage "test 2"  {
		when
		  Holder.Vars.Color.In("Black", "Yellow")
		then
		  Holder.Vars.Message = "Its either black or yellow!!!";
		  Retract("SetHolderVarMessage");
	}
	`

	// Prepare knowledgebase library and load it with our rule.
	lib := ast.NewKnowledgeLibrary()
	rb := builder.NewRuleBuilder(lib)
	err = rb.BuildRuleFromResource("Test", "0.1.1", pkg.NewBytesResource([]byte(rule)))
	assert.NoError(t, err)
	kb, err := lib.NewKnowledgeBaseInstance("Test", "0.1.1")
	assert.NoError(t, err)
	eng1 := &engine.GruleEngine{MaxCycle: 1}
	err = eng1.Execute(dataContext, kb)
	assert.NoError(t, err)
	assert.Equal(t, "Its either black or yellow!!!", holder.Vars["Message"])
}

func TestMapStringAccessor(t *testing.T) {
	vars := make(map[string]string)
	dataContext := ast.NewDataContext()
	err := dataContext.Add("Vars", vars)
	assert.NoError(t, err)

	rule := `
rule SetVarMessage "test 3" salience 1000 {
	when
	  true
	then
	  Vars.Color = "Black";
	  Retract("SetVarMessage");
}
rule Final salience 500 {
	when
	  Vars.Color.In("Black", "Yellow")
	then
	  Vars.Message = "Its either black or yellow!!!";
	  Retract("Final");
}
`

	// Prepare knowledgebase library and load it with our rule.
	lib := ast.NewKnowledgeLibrary()
	rb := builder.NewRuleBuilder(lib)
	err = rb.BuildRuleFromResource("Test", "0.1.1", pkg.NewBytesResource([]byte(rule)))
	assert.NoError(t, err)
	kb, err := lib.NewKnowledgeBaseInstance("Test", "0.1.1")
	assert.NoError(t, err)
	eng1 := &engine.GruleEngine{MaxCycle: 2}
	err = eng1.Execute(dataContext, kb)
	assert.NoError(t, err)
	assert.Equal(t, "Its either black or yellow!!!", vars["Message"])
}

func TestMapStringInterface(t *testing.T) {
	vars := make(map[string]interface{})
	dataContext := ast.NewDataContext()
	err := dataContext.Add("Vars", vars)
	dataContext.SetMissingKeyMaps([]string{"Vars"})
	assert.NoError(t, err)

	rule := `
rule SetVarMessage "test 4" salience 1000 {
	when
	  !Vars["Color"].In("Black", "Yellow")
	then
	  Vars["Color"] = "Black";
	  Retract("SetVarMessage");
}
rule Final salience 500 {
	when
	  Vars["Color"].In("Black", "Yellow")
	then
	  Vars.Message = "Its either black or yellow!!!";
	  Retract("Final");
}
`

	// Prepare knowledgebase library and load it with our rule.
	lib := ast.NewKnowledgeLibrary()
	rb := builder.NewRuleBuilder(lib)
	err = rb.BuildRuleFromResource("Test", "0.1.1", pkg.NewBytesResource([]byte(rule)))
	assert.NoError(t, err)
	kb, err := lib.NewKnowledgeBaseInstance("Test", "0.1.1")
	assert.NoError(t, err)
	eng1 := &engine.GruleEngine{MaxCycle: 2}
	err = eng1.Execute(dataContext, kb)
	assert.NoError(t, err)
	assert.Equal(t, "Its either black or yellow!!!", vars["Message"])
}

func TestMapArrayAppend(t *testing.T) {
	vars := make(map[string]interface{})
	dataContext := ast.NewDataContext()
	err := dataContext.Add("Vars", vars)
	dataContext.SetMissingKeyMaps([]string{"Vars"})
	assert.NoError(t, err)

	rule := `
rule AppendColor "test 5" salience 1000 {
	when
	  true 
	then
	  Vars.Color.Append("Black", "Yellow");
	  Retract("AppendColor");
}
rule Final salience 500 {
	when
	  "Yellow".In(Vars.Color)
	then
	  Vars.Message = "Its either black or yellow!!!";
	  Retract("Final");
}
`

	// Prepare knowledgebase library and load it with our rule.
	lib := ast.NewKnowledgeLibrary()
	rb := builder.NewRuleBuilder(lib)
	err = rb.BuildRuleFromResource("Test", "0.1.1", pkg.NewBytesResource([]byte(rule)))
	assert.NoError(t, err)
	kb, err := lib.NewKnowledgeBaseInstance("Test", "0.1.1")
	assert.NoError(t, err)
	eng1 := &engine.GruleEngine{MaxCycle: 2}
	err = eng1.Execute(dataContext, kb)
	assert.NoError(t, err)
	assert.Equal(t, "Its either black or yellow!!!", vars["Message"])
}

func TestMapNumericTypes(t *testing.T) {
	vars := make(map[string]interface{})
	dataContext := ast.NewDataContext()
	err := dataContext.Add("Vars", vars)
	dataContext.SetMissingKeyMaps([]string{"Vars"})
	assert.NoError(t, err)

	rule := `
rule SetVarArgs "test 6" salience 1000 {
	when
	  true
	then
	  Vars.IntArg = 2;
	  Vars.FloatArg = 2.5;
	  Retract("SetVarArgs");
}
rule SetVarMathArgs salience 500 {
	when
	  Vars.IntArg == 2 && Vars.FloatArg == 2.5
	then
	  Vars.Pow10 = Pow10(Vars.IntArg);
	  Vars.Floor = Floor(Vars.FloatArg);
	  Retract("SetVarMathArgs");
}
rule Final salience 250 {
	when
	  Vars.Pow10 == 100 && Vars.Floor == 2
	then
	  Vars.Message = "Math is fun!!!";
	  Retract("Final");
}
`

	// Prepare knowledgebase library and load it with our rule.
	lib := ast.NewKnowledgeLibrary()
	rb := builder.NewRuleBuilder(lib)
	err = rb.BuildRuleFromResource("Test", "0.1.1", pkg.NewBytesResource([]byte(rule)))
	assert.NoError(t, err)
	kb, err := lib.NewKnowledgeBaseInstance("Test", "0.1.1")
	assert.NoError(t, err)
	eng1 := &engine.GruleEngine{MaxCycle: 3}
	err = eng1.Execute(dataContext, kb)
	assert.NoError(t, err)
	assert.Equal(t, "Math is fun!!!", vars["Message"])
}

func TestMapArrayAppend2(t *testing.T) {
	vars := make(map[string]interface{})
	dataContext := ast.NewDataContext()
	vars["Color"] = []string{"Black"}
	err := dataContext.Add("Vars", vars)
	dataContext.SetMissingKeyMaps([]string{"Vars"})
	assert.NoError(t, err)

	rule := `
rule AppendColor salience 1000 {
	when
	  true 
	then
	  Vars.Color.Append("Yellow");
	  Retract("AppendColor");
}
rule Final salience 500 {
	when
	  "Yellow".In(Vars.Color)
	then
	  Vars.Message = "Its either black or yellow!!!";
	  Retract("Final");
}
`

	// Prepare knowledgebase library and load it with our rule.
	lib := ast.NewKnowledgeLibrary()
	rb := builder.NewRuleBuilder(lib)
	err = rb.BuildRuleFromResource("Test", "0.1.1", pkg.NewBytesResource([]byte(rule)))
	assert.NoError(t, err)
	kb, err := lib.NewKnowledgeBaseInstance("Test", "0.1.1")
	assert.NoError(t, err)
	eng1 := &engine.GruleEngine{MaxCycle: 2}
	err = eng1.Execute(dataContext, kb)
	assert.NoError(t, err)
	assert.Equal(t, "Its either black or yellow!!!", vars["Message"])
}

func TestMapMemoryClearing(t *testing.T) {
	vars := make(map[string]interface{})
	dataContext := ast.NewDataContext()
	vars["Color"] = []string{"Black"}
	err := dataContext.Add("Vars", vars)
	dataContext.SetMissingKeyMaps([]string{"Vars"})
	assert.NoError(t, err)

	rule := `
rule Final salience 1500 {
	when
		"Yellow".In(Vars.Color)
	then
		Vars.Message = "Its either black or yellow!!!";
		Complete();
}
rule AppendColor salience 1000 {
	when
	  true 
	then
	  Vars.Color.Append("Yellow");
	  Retract("AppendColor");
}
`

	// Prepare knowledgebase library and load it with our rule.
	lib := ast.NewKnowledgeLibrary()
	rb := builder.NewRuleBuilder(lib)
	err = rb.BuildRuleFromResource("Test", "0.1.1", pkg.NewBytesResource([]byte(rule)))
	assert.NoError(t, err)
	kb, err := lib.NewKnowledgeBaseInstance("Test", "0.1.1")
	assert.NoError(t, err)
	eng1 := &engine.GruleEngine{MaxCycle: 2}
	err = eng1.Execute(dataContext, kb)
	assert.NoError(t, err)
	assert.Equal(t, "Its either black or yellow!!!", vars["Message"])
}

func TestMapArrayRemove(t *testing.T) {
	vars := make(map[string]interface{})
	dataContext := ast.NewDataContext()
	err := dataContext.Add("Vars", vars)
	vars["Color"] = []string{"Black", "Yellow"}
	dataContext.SetMissingKeyMaps([]string{"Vars"})
	assert.NoError(t, err)

	rule := `
rule RemoveColor salience 1000 {
	when
	  true 
	then
	  Vars.Color.Remove("Yellow");
	  Retract("RemoveColor");
}
rule Final salience 500 {
	when
	  !"Yellow".In(Vars.Color)
	then
	  Vars.Message = "Its either black or yellow!!!";
	  Retract("Final");
}
`

	// Prepare knowledgebase library and load it with our rule.
	lib := ast.NewKnowledgeLibrary()
	rb := builder.NewRuleBuilder(lib)
	err = rb.BuildRuleFromResource("Test", "0.1.1", pkg.NewBytesResource([]byte(rule)))
	assert.NoError(t, err)
	kb, err := lib.NewKnowledgeBaseInstance("Test", "0.1.1")
	assert.NoError(t, err)
	eng1 := &engine.GruleEngine{MaxCycle: 2}
	err = eng1.Execute(dataContext, kb)
	assert.NoError(t, err)
	colors := vars["Color"]
	assert.ElementsMatch(t, []string{"Black"}, colors)
	assert.Equal(t, "Its either black or yellow!!!", vars["Message"])
}

func TestMapArrayClear(t *testing.T) {
	vars := make(map[string]interface{})
	dataContext := ast.NewDataContext()
	err := dataContext.Add("Vars", vars)
	vars["Color"] = []string{"Black", "Yellow"}
	dataContext.SetMissingKeyMaps([]string{"Vars"})
	assert.NoError(t, err)

	rule := `
rule ClearColor salience 1000 {
	when
	  true 
	then
	  Vars.Color.Clear();
	  Retract("ClearColor");
}
rule Final salience 500 {
	when
	  !"Yellow".In(Vars.Color)
	then
	  Vars.Message = "Its either black or yellow!!!";
	  Retract("Final");
}
`

	// Prepare knowledgebase library and load it with our rule.
	lib := ast.NewKnowledgeLibrary()
	rb := builder.NewRuleBuilder(lib)
	err = rb.BuildRuleFromResource("Test", "0.1.1", pkg.NewBytesResource([]byte(rule)))
	assert.NoError(t, err)
	kb, err := lib.NewKnowledgeBaseInstance("Test", "0.1.1")
	assert.NoError(t, err)
	eng1 := &engine.GruleEngine{MaxCycle: 2}
	err = eng1.Execute(dataContext, kb)
	assert.NoError(t, err)
	assert.Equal(t, "Its either black or yellow!!!", vars["Message"])
}

func TestMapBoolean(t *testing.T) {
	vars := make(map[string]interface{})
	dataContext := ast.NewDataContext()
	err := dataContext.Add("Vars", vars)
	vars["Color"] = []string{"Black", "Yellow"}
	dataContext.SetMissingKeyMaps([]string{"Vars"})
	assert.NoError(t, err)

	rule := `
rule SetPass salience 1000 {
	when
	  true 
	then
	  Vars.Pass = true;
	  Retract("SetPass");
}
rule Final salience 500 {
	when
	  Vars.Pass
	then
	  Vars.Message = "It's good!!!";
	  Retract("Final");
}
`

	// Prepare knowledgebase library and load it with our rule.
	lib := ast.NewKnowledgeLibrary()
	rb := builder.NewRuleBuilder(lib)
	err = rb.BuildRuleFromResource("Test", "0.1.1", pkg.NewBytesResource([]byte(rule)))
	assert.NoError(t, err)
	kb, err := lib.NewKnowledgeBaseInstance("Test", "0.1.1")
	assert.NoError(t, err)
	eng1 := &engine.GruleEngine{MaxCycle: 2}
	err = eng1.Execute(dataContext, kb)
	assert.NoError(t, err)
	assert.Equal(t, "It's good!!!", vars["Message"])
}

func TestMapBoolean2(t *testing.T) {
	vars := make(map[string]interface{})
	dataContext := ast.NewDataContext()
	err := dataContext.Add("Vars", vars)
	vars["Color"] = []string{"Black", "Yellow"}
	dataContext.SetMissingKeyMaps([]string{"Vars"})
	assert.NoError(t, err)

	rule := `
rule SetPass salience 1000 {
	when
	  true 
	then
	  Vars["Pass"] = true;
	  Retract("SetPass");
}
rule Final salience 500 {
	when
	  Vars["Pass"]
	then
	  Vars.Message = "It's good!!!";
	  Retract("Final");
}
`

	// Prepare knowledgebase library and load it with our rule.
	lib := ast.NewKnowledgeLibrary()
	rb := builder.NewRuleBuilder(lib)
	err = rb.BuildRuleFromResource("Test", "0.1.1", pkg.NewBytesResource([]byte(rule)))
	assert.NoError(t, err)
	kb, err := lib.NewKnowledgeBaseInstance("Test", "0.1.1")
	assert.NoError(t, err)
	eng1 := &engine.GruleEngine{MaxCycle: 2}
	err = eng1.Execute(dataContext, kb)
	assert.NoError(t, err)
	passEntry, found := vars["Pass"]
	assert.True(t, found)
	if found {
		assert.True(t, passEntry.(bool))
	}
	assert.Equal(t, "It's good!!!", vars["Message"])
}

type Helper struct{}

func (h *Helper) IsBlack(side string) bool {
	return side == "Black"
}

func TestMapComparisons(t *testing.T) {
	vars := make(map[string]interface{})
	dataContext := ast.NewDataContext()
	err := dataContext.Add("Vars", vars)
	assert.NoError(t, err)
	err = dataContext.Add("Helper", &Helper{})
	assert.NoError(t, err)
	vars["Color"] = []string{"Black", "Yellow"}
	dataContext.SetMissingKeyMaps([]string{"Vars"})
	assert.NoError(t, err)

	rule := `
rule SetSides salience 1000 {
	when
	  true 
	then
	  Vars.Left = "Yellow";
	  Vars.Right = "Black";
	  Retract("SetSides");
}
rule Final salience 500 {
	when
	  Helper.IsBlack(Vars.Right)
	then
	  Vars.Message = "It's good!!!";
	  Retract("Final");
}
`

	// Prepare knowledgebase library and load it with our rule.
	lib := ast.NewKnowledgeLibrary()
	rb := builder.NewRuleBuilder(lib)
	err = rb.BuildRuleFromResource("Test", "0.1.1", pkg.NewBytesResource([]byte(rule)))
	assert.NoError(t, err)
	kb, err := lib.NewKnowledgeBaseInstance("Test", "0.1.1")
	assert.NoError(t, err)
	eng1 := &engine.GruleEngine{MaxCycle: 2}
	err = eng1.Execute(dataContext, kb)
	assert.NoError(t, err)
	assert.Equal(t, "It's good!!!", vars["Message"])
}
