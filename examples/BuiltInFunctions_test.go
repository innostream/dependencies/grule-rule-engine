package examples

import (
	"testing"

	"gitlab.com/innostream/dependencies/grule-rule-engine/pkg"

	"gitlab.com/innostream/dependencies/grule-rule-engine/engine"

	"gitlab.com/innostream/dependencies/grule-rule-engine/ast"

	"gitlab.com/innostream/dependencies/grule-rule-engine/builder"

	"github.com/stretchr/testify/assert"
)

const (
	ConstructSliceRule = `
rule ConstructSlice "Construct a slice from multiple strings" {
	when
		"Test".In(Slice("Not", "Test"))
	then
		Log("Hello Grule");
		Retract("CallingLog");
}
`
)

func TestBuiltInSlice(t *testing.T) {
	dataContext := ast.NewDataContext()

	lib := ast.NewKnowledgeLibrary()
	ruleBuilder := builder.NewRuleBuilder(lib)
	err := ruleBuilder.BuildRuleFromResource("BuiltInSlice", "0.1.1", pkg.NewBytesResource([]byte(GRL)))
	assert.NoError(t, err)

	knowledgeBase, err := lib.NewKnowledgeBaseInstance("BuiltInSlice", "0.1.1")
	assert.NoError(t, err)

	eng1 := &engine.GruleEngine{MaxCycle: 1}
	err = eng1.Execute(dataContext, knowledgeBase)
	assert.NoError(t, err)
}
