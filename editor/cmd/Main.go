package main

import (
	"gitlab.com/innostream/dependencies/grule-rule-engine/editor"

	"github.com/sirupsen/logrus"
)

func main() {
	logrus.SetLevel(logrus.TraceLevel)
	editor.Start()
}
